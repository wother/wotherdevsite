import { $$,$$$ } from "./lib/selectors.js"
import { elt } from "./lib/elt.js"

//listners

$$("#addSelectorButton").addEventListener('click', function(){
    let radioDivs = $$$(".radioDiv");
    // Subtracting Three here to fix our fencepost issue.
    let numberOfCurrentInputs = radioDivs.item(0).childNodes.length - 3;
    radioDivs.forEach(selectedDiv => {
        let inputOptions = {
            name: `${ordinalLetters[numberOfCurrentInputs]}${selectedDiv.dataset.type}`,
            type: "radio"
        };
        let newInput = elt('input', inputOptions);
        selectedDiv.appendChild(newInput);
    });
});

$$("#removeSelectorButton").addEventListener('click', function(){
    let radioDivs = $$$(".radioDiv");
    radioDivs.forEach(selectedDiv => {
        if (selectedDiv.childNodes.length - 3 > 0) {
            selectedDiv.removeChild(selectedDiv.lastChild);
        }
    });
});

// Data sets

const ordinalLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");