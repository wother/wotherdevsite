/**
 * Our wrapper for createElement
 * @param {String} tag Name of the HTML Element
 * @param {Object} options Passthrough Options Object
 */
export function elt (tag, options) {
    let outputElement = document.createElement(tag);
    if (options) {
        Object.keys(options).forEach(key =>{
            outputElement[key] = options[key];
        });
    };
    return outputElement;
};